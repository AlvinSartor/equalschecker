/*
Copyright 2019 Alvin Sartor

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

using System.Collections.Immutable;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.Diagnostics;

namespace EqualsChecker
{
    [DiagnosticAnalyzer(LanguageNames.CSharp)]
    public sealed class EqualsCheckerAnalyzer : DiagnosticAnalyzer
    {
        public const string DiagnosticId = "EqualsCheckerAnalyzer";

        private const string Title = "Equality between references with '==' or '!=' symbols";
        private const string Description = "Using '==' or '!=' to check reference equality is bug prone. Either use ReferenceEquals or Equals.";
        private const string MessageFormat = "References are compared using '==' or '!='.";
        private const string Category = "Usage";

        private static readonly DiagnosticDescriptor Rule = new DiagnosticDescriptor(DiagnosticId, Title, MessageFormat, Category, DiagnosticSeverity.Error, true, Description);

        public override ImmutableArray<DiagnosticDescriptor> SupportedDiagnostics => ImmutableArray.Create(Rule);

        /// <summary>
        /// Called once at session start to register actions in the analysis context.
        /// </summary>
        public override void Initialize(AnalysisContext context)
        {
            context.RegisterSyntaxNodeAction(AnalyzeNode, SyntaxKind.EqualsExpression);
            context.RegisterSyntaxNodeAction(AnalyzeNode, SyntaxKind.NotEqualsExpression);
        }

        private static void AnalyzeNode(SyntaxNodeAnalysisContext context)
        {
            var binaryExpression = (BinaryExpressionSyntax)context.Node;
            if (Skip(binaryExpression, context.SemanticModel)) return;

            var diagnostic = Diagnostic.Create(Rule, binaryExpression.GetLocation());
            context.ReportDiagnostic(diagnostic);
        }

        private static bool Skip(BinaryExpressionSyntax binaryExpression, SemanticModel semanticModel)
        {
            bool BothAreValues(ITypeSymbol l, ITypeSymbol r) => l.IsValueType && r.IsValueType;
            
            bool NodeIsString(ITypeSymbol n) => n.SpecialType == SpecialType.System_String;
            bool BothAreStrings(ITypeSymbol l, ITypeSymbol r) => NodeIsString(l) && NodeIsString(r);

            bool NodeIsTypeOf(ExpressionSyntax n) => n is TypeOfExpressionSyntax;
            bool BothAreTypeOf(BinaryExpressionSyntax binary) => NodeIsTypeOf(binary.Left) && NodeIsTypeOf(binary.Right);

            bool NodeIsType(ITypeSymbol n) => n.Name == "Type";
            bool BothAreTypes(ITypeSymbol l, ITypeSymbol r) => NodeIsType(l) && NodeIsType(r);


            bool NodeIsError(ITypeSymbol n) => n.OriginalDefinition is IErrorTypeSymbol;
            bool AnyIsError(ITypeSymbol l, ITypeSymbol r) => NodeIsError(l) || NodeIsError(r);

            bool AnyIsNull(ITypeSymbol l, ITypeSymbol r) => l is null || r is null;

            var left = semanticModel.GetTypeInfo(binaryExpression.Left).Type;
            var right = semanticModel.GetTypeInfo(binaryExpression.Right).Type;

            return AnyIsNull(left, right)
                   || AnyIsError(left, right)
                   || BothAreValues(left, right)
                   || BothAreStrings(left, right)
                   || BothAreTypeOf(binaryExpression)
                   || BothAreTypes(left, right);
        }
    }
}
