using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.Diagnostics;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TestHelper;

namespace EqualsChecker.Test
{
    [TestClass]
    public sealed class UnitTest : CodeFixVerifier
    {
        [TestMethod]
        public void EmptyDocumentCausesNoIssue()
        {
            var test = @"";

            VerifyCSharpDiagnostic(test);
        }

        [TestMethod]
        public void TypeOfIsIgnored()
        {
            var test = @"
    namespace ConsoleApplication1
    {
        class TypeName
        {       
            public void TypeOfComparison()
            {
                if (typeof(string) == typeof(int)) return;  
                if (typeof(string) != typeof(int)) return;  
                if (GetType() == string.Empty.GetType();) return;
                if (GetType() != string.Empty.GetType();) return;
            }

        }
    }";

            VerifyCSharpDiagnostic(test);
        }

        [TestMethod]
        public void NullIsIgnored()
        {
            var test = @"
    namespace ConsoleApplication1
    {
        class TypeName
        {       
            public void TypeOfComparison()
            {
                var o = new object();

                if (o == null) return;  
                if (o != null) return;  
                if (null == o) return;  
                if (null != o) return;  
            }

        }
    }";

            VerifyCSharpDiagnostic(test);
        }

        [TestMethod]
        public void ValueTypesAreIgnored()
        {
            var test = @"
    namespace ConsoleApplication1
    {
        class TypeName
        {       
            public void ValueTypeComparison()
            {
                int i1 = 12;
                int i2 = 12;

                if (i1 == i2) return; 
                if (i1 != i2) return; 
            }

        }
    }";

            VerifyCSharpDiagnostic(test);
        }

        [TestMethod]
        public void StringTypesAreIgnored()
        {
            var test = @"
    namespace ConsoleApplication1
    {
        class TypeName
        {       
            public void StringsComparison()
            {
                string s1 = 1.ToString();
                string s2 = 12.ToString();

                if (s1 == s2) return;   
                if (s1 != s2) return;   
            }

        }
    }";

            VerifyCSharpDiagnostic(test);
        }

        [TestMethod]
        public void ValuePropertiesAreIgnored()
        {
            var test = @"
    using System.Collections.Immutable;
    
    namespace ConsoleApplication1
    {
        class TypeName
        {       
            public int ValuePropertyComparison()
            {
                return ImmutableList<int>.Empty.Count == 0;
                return ImmutableList<int>.Empty.Count != 0;
            }

        }
    }";

            VerifyCSharpDiagnostic(test);
        }

        //Diagnostic and CodeFix both triggered and checked for
        [TestMethod]
        public void ReferencesAreSpottedAndMarkedWithError()
        {
            var test = @"
    namespace ConsoleApplication1
    {
        class TypeName
        {   
            public void V1()
            {
                object o1 = new object();
                object o2 = new object();

                if (o1 == o2) return;
                if (o1 != o2) return;
            }          
        }
    }";
            var expected1 = new DiagnosticResult
            {
                Id = "EqualsCheckerAnalyzer",
                Message = "References are compared using '==' or '!='.",
                Severity = DiagnosticSeverity.Error,
                Locations = new[] {new DiagnosticResultLocation("Test0.cs", 11, 21)}
            };
            var expected2 = new DiagnosticResult
            {
                Id = "EqualsCheckerAnalyzer",
                Message = "References are compared using '==' or '!='.",
                Severity = DiagnosticSeverity.Error,
                Locations = new[] {new DiagnosticResultLocation("Test0.cs", 12, 21)}
            };

            VerifyCSharpDiagnostic(test, expected1, expected2);
        }

        protected override DiagnosticAnalyzer GetCSharpDiagnosticAnalyzer()
        {
            return new EqualsCheckerAnalyzer();
        }
    }
}
